<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Upload disk
    |--------------------------------------------------------------------------
    |
    | Specify the storage disk where to upload the temporary images
    |
    */
    'disk' => 'local',

    /*
    |--------------------------------------------------------------------------
    | Upload path
    |--------------------------------------------------------------------------
    |
    | Specify the storage path where to upload the temporary images
    |
    */
    'path' => 'image_uploads',

    /*
    |--------------------------------------------------------------------------
    | URL route
    |--------------------------------------------------------------------------
    |
    | Specify the route you want to use for the temporary images
    | Set to null if you want to create your own route and controller
    |
    */
    'route' => 'int/image-upload'
];
