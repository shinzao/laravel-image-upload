<?php
namespace Enso\ImageUpload;

use Illuminate\Support\ServiceProvider;
use Enso\ImageUpload\Controllers\ImageUploadController;

class ImageUploadServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/image-upload.php' => config_path('image-upload.php'),
            __DIR__.'/resources' => resource_path()
        ]);
    }
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/image-upload.php', 'image-upload');

        $this->app->bind('image-upload', function() {
            return new ImageUploadController();
        });

        if (is_string(config('image-upload.route'))) {
            $this->app['router']->resource(config('image-upload.route'), 'Enso\ImageUpload\Controllers\ImageUploadController', [
                'only' => ['store', 'destroy'],
                'middleware' => ['web']
            ]);
        }
    }
}
