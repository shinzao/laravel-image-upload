<?php
namespace Enso\ImageUpload\Facades;

use Illuminate\Support\Facades\Facade;

class ImageUpload extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'image-upload';
    }
}
