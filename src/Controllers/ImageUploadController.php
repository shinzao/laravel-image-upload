<?php
namespace Enso\ImageUpload\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Storage;
use Illuminate\Routing\Controller as BaseController;

class ImageUploadController extends BaseController
{
    private static $disk;

    /**
     * ImageUploadController constructor.
     */
    public function __construct()
    {
        static::$disk = Storage::disk(config('image-upload.disk'));
    }

    /**
     * Get uploaded images
     *
     * @param String|null $id uploader id
     * @param Bool $inline return uploaded images as inline data
     * @return array
     */
    private function get(String $id = null)
    {
        $uploads = static::$disk->files(csrf_token());

        if (is_null($id)) {
            return $uploads;
        }

        $files = [];

        foreach($uploads as $uploadFile) {
            $fileName = substr(strrchr($uploadFile, '/'), 1);

            if (strtok($fileName, '-') == $id) {
                $files[$fileName] = $uploadFile;
            }
        }

        return $files;
    }

    /**
     * Get uploaded images as UploadedFile entities
     *
     * @param String|null $id
     * @return array
     */
    public function getFiles(String $id = null)
    {
        $files = [];

        foreach($this->get($id) as $filename => $path) {
            $files[] = $this->generateUploadedFile($path, $filename);
        }

        return $files;
    }

    /**
     * Get uploaded images as json inline data source
     *
     * @param String|null $id
     * @param Array|null $files
     * @return string
     */
    public function getJson(String $id = null, Array $files = null)
    {
        $json = [];

        foreach($this->get($id) as $filename => $path) {
            $json[$filename] = [
                'type' => 'temp',
                'data' => $this->generateSource(static::$disk->get($path))
            ];
        }

        if (!is_null($files)) {
            foreach($files as $file) {
                if (file_exists($file)) {
                    $json[$file] = [
                        'type' => 'file',
                        'data' => $this->generateSource(file_get_contents($file))
                    ];
                }
            }
        }

        return json_encode($json, JSON_FORCE_OBJECT);
    }

    /**
     * Converts image entry to inline data source
     *
     * @param String $file
     * @return string
     */
    private function generateSource(String $binary)
    {
        $finfo = new \finfo(FILEINFO_MIME_TYPE);

        return sprintf(
            'data:%s;base64,%s',
            $finfo->buffer($binary),
            base64_encode($binary)
        );
    }

    /**
     * Converts image entry to UploadedFile entity
     *
     * @param String $path
     * @param String $filename
     * @return UploadedFile
     */
    private function generateUploadedFile(String $path, String $filename)
    {
        $fullPath = static::$disk->getDriver()->getAdapter()->getPathPrefix() . $path;

        return new UploadedFile($fullPath, $filename, mime_content_type($fullPath), filesize($fullPath));
    }

    /**
     * Stores all uploaded images to the temporary folder
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request)
    {
        foreach($request->file('images') as $id => $file) {
            $file->storeAs(csrf_token(), $id, config('image-upload.disk'));
        }

        return response(null, 200);
    }

    /**
     * Removes an image from the temporary folder
     *
     * @param Request $request
     * @param String $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function destroy(Request $request, String $id)
    {
        if ($request->get('type') === 'temp') {
            if (static::$disk->delete(csrf_token() . '/' . $request->get('id'))) {
                if (count(static::$disk->files(csrf_token())) == 0) {
                    static::$disk->deleteDirectory(csrf_token());
                }

                return response(null, 200);
            }
        } else {
            if (unlink($request->get('id'))) {
                return response(null, 200);
            }
        }

        return response(null, 400);
    }

    /**
     * Resets/deletes temporary image uploads
     *
     * @param String|null $id uploader id
     */
    public function reset(String $id = null)
    {
        static::$disk->delete($this->get($id));

        if (is_null($id) || count(static::$disk->files(csrf_token())) == 0) {
            static::$disk->deleteDirectory(csrf_token());
        }
    }
}
