
# Laravel image upload

This package adds a vue component for image uploads with ajax and image previews.  
It also adds the corresponding backend functionality to work with the Laravel storage and filesystem. 

## Requirements
- PHP >= 7.0
- Laravel >= 5.4
- Vue.js >= 2.1
- Axios >= 0.15

## Installation

Add the package to your  `composer.json`:
```
composer require enso/image-upload
```

## Setup

Add this line to the providers array in `config/app.php`:
```
Enso\ImageUpload\ImageUploadServiceProvider::class,
```
Add this line to the aliases array in `config/app.php`:
```
'ImageUpload' => Enso\ImageUpload\Facades\ImageUpload::class,
```
Publish the package configuration and the vue component (it gets saved to the default laravel folder: `resources/assets/js/components/ImageUpload.vue`):
```
php artisan vendor:publish
```
Change the configuration file `config/image-upload.php` if necessary.

## Usage

### Backend

You can use the following methods on the `ImageUpload` facade in your controllers:

`ImageUpload::getFiles(String $id = null)`  
Get the uploaded images as an array of Laravel `UploadedFile` entities
- Set the `$id` parameter to your uploader id. If this parameter is null, the files of all uploaders are returned.

`ImageUpload::getJson(String $id = null, Array $files = null)`  
Get the temporary uploaded images (and optional existing files) as an inline data source. 
- Set the `$id` parameter to your uploader id. If this parameter is null, the files of all uploaders are returned.  
- Set the `$files` parameter to an array of file paths. You can use this parameter to add and remove already existing files within the uploader. 

`ImageUpload::reset(String $id = null)`  
Resets/deletes the temporary image uploads. 
- Set the `$id` paremeter to your uploader id. If this parameter is null, the files of all uploaders gets deleted.

### Frontend

Require the vue component and use it like this in your templates.
```
<image-upload :id="avatar" :images="{{ \ImageUpload::getJson('avatar', [storage_path('images/avatars/'.$user->id.'.jpg')]) }}"></image-upload>
```
This example will create an image uploader with the id "avatar", which also initially loads an existing image.

You can use the following parameters on the vue component:  
- `id`: Sets the id of your uploader
- `max`: Sets the maximum number of images you can upload
- `maxSize`: Sets the allowed maximum file size for an image. By default it is set to 2097152 (2 MB)
- `wrapper-class`: The class used for the wrapping container of an single image. By default its set to bootstraps "col-sm-2"
- `images`: Sets the preloaded images for this uploader. Use this parameter together with the backend getJson() method (see example)

## Author

Daniel Hoffmann | [Enso](http://enso.re)